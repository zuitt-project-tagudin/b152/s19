let fivePow3 = Math.pow(5,3);
console.log(fivePow3);

// Exponent operator === **

console.log(5**3);

let fivePow2 = 5**2;
let fivePow4 = 5**4;
let twoPow2 = 2**2

console.log(fivePow2);
console.log(fivePow4);
console.log(twoPow2);



function squareRootChecker(num) {
	return num ** .5;
}
let squareRootOf4 = squareRootChecker(4);
console.log(squareRootOf4);

/*
	Template literals ==  ``   backticks
*/

let sampStr1 = `Charlie`;
let sampStr2 = `Joy`;

console.log(sampStr1);
console.log(typeof sampStr2);

//let combinedStr =  sampStr1 + " " + sampStr2;
 let combinedStr = `${sampStr1} ${sampStr2}`;
console.log(combinedStr);

let num1 = 15;
let num2 = 3;

let sentence = `the result of ${num1} plus ${num2} is ${num1 + num2}`
console.log(sentence);

let sentence2 = `the result of ${num1} to the power of 2 is ${num1 ** 2}`;
let sentence3 = `the result of ${num2} times 6 is ${num2 * 6}`;

console.log(sentence2);
console.log(sentence3);

/*
	Array Destructuring
*/

let array = ["kobe", "lebron", "jordan"]

/*let goat1 = array[0];
let goat2 = array[1];
let goat3 = array[2];

console.log(goat1, goat2, goat3);*/

let [goat1, goat2, goat3] = array;
console.log(goat1);
console.log(goat2);
console.log(goat3);

let array2 = ["curry", "lillard", "paul", "irving"];

let [pg1, pg2, pg3, pg4] = array2;
console.log(pg1);
console.log(pg2);
console.log(pg3);
console.log(pg4);

let array3 = ["jokic", "embiid", "anthony-towns", "gobert"];

let [center1,, center3] = array3;
console.log(center1);
console.log(center3);

let array4 = ["draco malfoy", "hermione granger", "harry potter", "ron weasley", "professor snape"];

let [, gryffindor1, gryffindor2, gryffindor3] = array4;
console.log(gryffindor1);
console.log(gryffindor2);
console.log(gryffindor3);

const [slytherin1,,,,slytherin2] = array4;
console.log(slytherin1)
console.log(slytherin2)

gryffindor1 = "emma watson";
console.log(gryffindor1);

/*
	Object Destructuring
*/

let pokemon = {
	name: "Blastoise",
	level: 40,
	health: 80
}

let sentence4 = `the pokemon's name is ${pokemon.name}`;
let sentence5 = `It is a level ${pokemon.level} pokemon`;
let sentence6 = `It has at least ${pokemon.health} health points`;

console.log(sentence4);
console.log(sentence5);
console.log(sentence6);

let {health, name, level} = pokemon;
console.log(health);
console.log(name);
console.log(level);


let person = {
	name: "Paul Phoenix",
	age: 31,
	birthday: "January 12, 1991"
}

function greet(obj) {
	let {name, age, birthday} = obj;
	console.log(`Hi! my name is ${name}`)
	console.log(`I am ${age} years old`)
	console.log(`My birthday is on ${birthday}`)
}
greet(person);

let actors = ["tom hanks", "leonardo dicaprio", "anthony hopkins", "ryan reynolds"];

let director = {
	name: "alfred hitchcock",
	birthday: "august 13, 1889",
	isActive: false,
	movies: [" The Birds", "Psycho", "North by Northwest"]
}

let [actor1, actor2, actor3] = actors;

function displayDirector({name, birthday, movies, isActive}) {
	// let {name, birthday, movie, isActive} = person;

	console.log(`The Directors name is ${name}`);
	console.log(`He was born on ${birthday}`);
	console.log(`His movies include: `);
	console.log(movies);

}

console.log(actor1);
console.log(actor2);
console.log(actor3);

displayDirector(director);

/*
	Arrow functions
	

*/

const hello = () => {
	console.log("sadas");
}

console.log(hello());

const greets = (personParams) => {
	console.log(`hi, ${personParams.name}!`)
}

greets(person);

actors.forEach(actor=>console.log(actor));

let numArr = [1, 2, 3, 4, 5, 6, 7, 8];

let times5 = numArr.map((num) => num*5);

console.log(times5);


;

// implicit return: one lineer functions with no curly braces and automatically returns
const addNum = (a, b) => a + b;
const subtractNum = (a, b) => a - b;

let difference1 = subtractNum(45, 15);
console.log(difference1);


let sumExample = addNum(50, 10);
console.log(sumExample);


let protag = {
	name: "Cloud Strife",
	occupation: "soldier",
	greet: function() {
		console.log(this);
		console.log(this.name);
	},
	introduceJob: () => {
		console.log(this);
		console.log(`i work as a ${this.occupation}`);
	}
}

protag.greet();
protag.introduceJob();


/*class based object blueprints*/

function Pokemon(name, type, level) {
	this.name = name;
	this.type = type;
	this.level = level;
};

let pokemon1 = new Pokemon("Sandshrew", "Ground", 15)
console.log(pokemon1);

class Car {
	constructor(brand, name, year) {
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}


let car1 = new Car("Toyota", "Vios", "2002")
console.log(car1);